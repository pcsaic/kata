package com.example.kata.solid.example.eD;

public class LocalStorage implements FetchClient{

    @Override
    public String fetch() {
        return "Fetching from local storage!";
    }
}
