package com.example.kata.solid.example.dI.wrong.animal;

public class Eagle implements Animal{
    @Override
    public String fly() {
        return "YAAA I like it!";
    }

    @Override
    public String swim() {
        return "Holly cakes... it is impossible!";
    }

    @Override
    public String walk() {
        return "YAAA I like it!";
    }

    @Override
    public String breathe() {
        return "YAAA I like it!";
    }
}
