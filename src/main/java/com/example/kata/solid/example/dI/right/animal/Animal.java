package com.example.kata.solid.example.dI.right.animal;

public interface Animal {

    String breathe();
}
