package com.example.kata.solid.example.bO;

public interface Shape {
    Double area();
}
