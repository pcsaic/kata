package com.example.kata.solid.example.bO.shape;

import com.example.kata.solid.example.bO.Shape;

public class Square implements Shape {

    private final Double size;

    public Square(Double size){
        this.size = size;
    }

    @Override
    public Double area() {
        return Math.pow(size, 2);
    }
}
