package com.example.kata.solid.example.dI.right;

//interface segregation principle
//class should not inherit interfaces with extra methods

import com.example.kata.solid.example.dI.right.animal.implementation.Dog;
import com.example.kata.solid.example.dI.right.animal.implementation.Eagle;
import com.example.kata.solid.example.dI.right.animal.implementation.Whale;

public class InterfaceSegregation {
    public static void main(String[] args) {

        var eagle = new Eagle();
        var whale = new Whale();
        var dog = new Dog();

        System.out.println("Eagle can:");
        System.out.println(eagle.breathe());
        System.out.println(eagle.fly());
        System.out.println(eagle.walk());

        System.out.println("Whale can:");
        System.out.println(whale.breathe());
        System.out.println(whale.swim());

        System.out.println("Dog can:");
        System.out.println(dog.breathe());
        System.out.println(dog.walk());
        System.out.println(dog.swim());
    }
}

