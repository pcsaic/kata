package com.example.kata.solid.example.dI.right.animal.implementation;

import com.example.kata.solid.example.dI.right.animal.Swimmer;
import com.example.kata.solid.example.dI.right.animal.Walker;

public class Dog implements Walker, Swimmer {
    @Override
    public String breathe() {
        return "Dog is breathing!";
    }

    @Override
    public String walk() {
        return "Dog is walking!";
    }

    @Override
    public String swim() {
        return "Dog is swimming!";
    }
}
