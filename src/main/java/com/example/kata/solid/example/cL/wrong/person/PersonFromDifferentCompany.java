package com.example.kata.solid.example.cL.wrong.person;

public class PersonFromDifferentCompany extends Person {

    @Override
    public boolean hasAccess() {
        return false;
    }
}
