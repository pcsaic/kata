package com.example.kata.solid.example.cL.right.person;

public abstract class Person {

    public boolean canTalk(){
        return true;
    }

    public abstract boolean hasAccess();
}
