package com.example.kata.solid.example.aS;

public class NewsPrinter {

    private final News news;

    public String toJson() {
        return "{"+
                "\"title\":" + news.getTitle() + ","+
                "\"text\":" + news.getText() +
                "}";
    }

    public String toHtml() {
        return "<div class='news'>" +
                "<h1>" + news.getTitle() + "</h1>" +
                "<p>" + news.getText() + "</p>" +
                "</div>";
    }

    NewsPrinter(News news) {
        this.news = news;
    }
}
