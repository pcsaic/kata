package com.example.kata.solid.example.eD;

public class Repository {

    private final FetchClient fetchClient;

    public String getFromRepository(){
        return fetchClient.fetch();
    }

    public Repository(FetchClient fetchClient) {
        this.fetchClient = fetchClient;
    }
}
