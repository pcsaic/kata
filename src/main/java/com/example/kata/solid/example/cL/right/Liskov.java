package com.example.kata.solid.example.cL.right;

import com.example.kata.solid.example.cL.right.person.Backend;
import com.example.kata.solid.example.cL.right.person.Frontend;
import com.example.kata.solid.example.cL.right.person.PersonFromDifferentCompany;

public class Liskov {
    public static void main(String[] args) {

        Office office = new Office();

        Backend backendDeveloper = new Backend();
        Frontend frontendDeveloper = new Frontend();
        PersonFromDifferentCompany spy = new PersonFromDifferentCompany();

        System.out.println("For backend developer: "+office.openDoor());
        System.out.println("For backend developer: "+office.openSecretDoor(backendDeveloper));

        System.out.println("For frontend developer: "+office.openDoor());
        System.out.println("For frontend developer: "+office.openSecretDoor(frontendDeveloper));

        System.out.println("For spy: "+office.openDoor());
        System.out.println("For spy: "+office.openSecretDoor(spy));
    }
}
