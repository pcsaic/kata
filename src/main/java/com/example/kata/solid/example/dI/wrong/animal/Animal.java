package com.example.kata.solid.example.dI.wrong.animal;

public interface Animal {
    String fly();
    String swim();
    String walk();
    String breathe();
}

