package com.example.kata.solid.example.dI.right.animal.implementation;

import com.example.kata.solid.example.dI.right.animal.Swimmer;

public class Whale implements Swimmer {
    @Override
    public String breathe() {
        return "Whale is breathing!";
    }

    @Override
    public String swim() {
        return "Whale is swimming!";
    }
}
