package com.example.kata.solid.example.aS;

public class News {
    private final String title;
    private final String text;

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    News(String title, String text) {
        this.title = title;
        this.text = text;
    }
}
