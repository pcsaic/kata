package com.example.kata.solid.example.dI.right.animal;

public interface Bird extends Animal{

    String fly();
}
