package com.example.kata.solid.example.bO.shape;

import com.example.kata.solid.example.bO.Shape;

public class Circle implements Shape {

    private final Double radius;

    public Circle(Double radius) {
        this.radius = radius;
    }

    @Override
    public Double area() {
        return Math.pow(radius, 2) * Math.PI;
    }
}
