package com.example.kata.solid.example.cL.right;

import com.example.kata.solid.example.cL.right.person.Person;

public class Office {

    public String openDoor(){
        return "Door is open";
    }

    public String openSecretDoor(Person person){
        if(person.hasAccess()){
            return "Secret door is open!";
        }
        else return "No access! Secret door is close!";
    }

    public Office() {
    }
}
