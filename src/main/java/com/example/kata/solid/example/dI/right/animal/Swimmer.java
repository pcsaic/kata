package com.example.kata.solid.example.dI.right.animal;

public interface Swimmer extends Animal{

    String swim();
}
