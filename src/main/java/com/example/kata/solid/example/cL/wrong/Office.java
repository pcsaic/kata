package com.example.kata.solid.example.cL.wrong;

import com.example.kata.solid.example.cL.wrong.person.Person;

public class Office {

    private Person person;

    public String openDoor(){
        return "Door is open";
    }

    public String openSecretDoor(){
        if(person.hasAccess()){
            return "Secret door is open!";
        }
        else return "No access! Secret door is close!";
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Office() {
    }

    public Office(Person person) {
        this.person = person;
    }
}
