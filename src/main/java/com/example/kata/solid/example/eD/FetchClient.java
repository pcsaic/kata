package com.example.kata.solid.example.eD;

public interface FetchClient {

    String fetch();

}
