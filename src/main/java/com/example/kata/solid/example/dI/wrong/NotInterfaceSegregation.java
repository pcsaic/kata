package com.example.kata.solid.example.dI.wrong;

import com.example.kata.solid.example.dI.wrong.animal.Eagle;
import com.example.kata.solid.example.dI.wrong.animal.Dog;
import com.example.kata.solid.example.dI.wrong.animal.Whale;

//interface segregation principle
//class should not inherit interfaces with extra methods

public class NotInterfaceSegregation {
    public static void main(String[] args) {

        var bird = new Eagle();
        var dog = new Dog();
        var whale = new Whale();

        System.out.println("Eagle actions:");
        System.out.println(bird.breathe());
        System.out.println(bird.fly());
        System.out.println(bird.swim());

        System.out.println("Dog action:");
        System.out.println(dog.breathe());
        System.out.println(dog.fly());
        System.out.println(dog.swim());

        System.out.println("Whale action:");
        System.out.println(whale.breathe());
        System.out.println(whale.fly());
        System.out.println(whale.swim());
    }
}
