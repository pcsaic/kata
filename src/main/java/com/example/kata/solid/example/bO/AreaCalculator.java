package com.example.kata.solid.example.bO;

import java.util.Arrays;
import java.util.List;

public class AreaCalculator {

    private final List<Shape> shapes;

    public Double sum(){
        return shapes.stream().mapToDouble(Shape::area).sum();
    }

    AreaCalculator(Shape... shapes) {
        this.shapes= Arrays.asList(shapes);
    }
}
