package com.example.kata.solid.example.dI.right.animal;

public interface Walker extends Animal{

    String walk();
}
