package com.example.kata.solid.example.eD;

public class DependencyInversion {

    public static void main(String[] args) {

        Repository repository1 = new Repository(new DatabaseStorage());

        System.out.println(repository1.getFromRepository());

        Repository repository2 = new Repository(new LocalStorage());

        System.out.println(repository2.getFromRepository());

    }
}
