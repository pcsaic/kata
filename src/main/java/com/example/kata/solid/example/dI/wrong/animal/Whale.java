package com.example.kata.solid.example.dI.wrong.animal;

public class Whale implements Animal{
    @Override
    public String fly() {
        return "Ouch it is impossible!";
    }

    @Override
    public String swim() {
        return "YAAA I like it!";
    }

    @Override
    public String walk() {
        return "Holly cakes... it is impossible!";
    }

    @Override
    public String breathe() {
        return "YAAAA I like it!";
    }
}
