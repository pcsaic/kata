package com.example.kata.solid.example.dI.right.animal.implementation;

import com.example.kata.solid.example.dI.right.animal.Bird;
import com.example.kata.solid.example.dI.right.animal.Walker;

public class Eagle implements Bird, Walker {
    @Override
    public String breathe() {
        return "Eagle is breathing!";
    }

    @Override
    public String fly() {
        return "Eagle is flying";
    }

    @Override
    public String walk() {
        return "Eagle is walking!";
    }
}
