package com.example.kata.solid.example.cL.wrong;

//The inheriting class should complement, not replace, the behavior of the base class.

import com.example.kata.solid.example.cL.wrong.person.Backend;
import com.example.kata.solid.example.cL.wrong.person.Frontend;
import com.example.kata.solid.example.cL.wrong.person.PersonFromDifferentCompany;

public class NotLiskov {
    public static void main(String[] args) {

        Office office = new Office();

        Backend backendDeveloper = new Backend();
        Frontend frontendDeveloper = new Frontend();
        PersonFromDifferentCompany spy = new PersonFromDifferentCompany();

        office.setPerson(backendDeveloper);
        System.out.println("For backend developer: "+office.openDoor());
        System.out.println("For backend developer: "+office.openSecretDoor());

        office.setPerson(frontendDeveloper);
        System.out.println("For frontend developer: "+office.openDoor());
        System.out.println("For frontend developer: "+office.openSecretDoor());

//        wrong block... because spy override access
//        according with Liskov use right level of abstractions
        office.setPerson(spy);
        System.out.println("For spy: "+office.openDoor());
        System.out.println("For spy: "+office.openSecretDoor());

    }
}
