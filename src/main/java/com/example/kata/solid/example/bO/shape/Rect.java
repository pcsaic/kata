package com.example.kata.solid.example.bO.shape;

import com.example.kata.solid.example.bO.Shape;

public class Rect implements Shape {
    private final Double with;
    private final Double height;

    public Rect(Double with, Double height){
        this.with = with;
        this.height = height;
    }

    @Override
    public Double area() {
        return with * height;
    }
}
