package com.example.kata.solid.example.aS;

public class Single {

    public static void main(String[] args) {
        News news = new News("Single", "Single responsibility principal.");
        NewsPrinter printer = new NewsPrinter(news);

        System.out.println(printer.toHtml());
        System.out.println(printer.toJson());
    }
}
