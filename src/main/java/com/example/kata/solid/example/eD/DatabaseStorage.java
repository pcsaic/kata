package com.example.kata.solid.example.eD;

public class DatabaseStorage implements FetchClient{

    @Override
    public String fetch() {
        return "Fetching from database!";
    }
}
