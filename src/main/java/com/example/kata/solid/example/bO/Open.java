package com.example.kata.solid.example.bO;

import com.example.kata.solid.example.bO.shape.Circle;
import com.example.kata.solid.example.bO.shape.Rect;
import com.example.kata.solid.example.bO.shape.Square;

public class Open {

    //Open Close principle
    public static void main(String[] args) {

        AreaCalculator calc = new AreaCalculator(
                new Circle(2.0),
                new Rect(3.0, 5.0),
                new Square(4.0)
        );

        System.out.println(calc.sum());
    }
}

